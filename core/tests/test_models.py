from django.test import TestCase
from django.contrib.auth import get_user_model

from core import models


def sample_user(email='test@gmail.com', password="test123"):
    "create sample user"
    return get_user_model().objects.create_user(email, password)


class ModelTests(TestCase):

    def test_create_user_with_email_successful(self):
        """ Test creating a new users with an email is successful """
        email = 'atombombmount@gmail.com'
        password = 'test@8848'
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalize(self):
        """ test the new email is normalize  """
        email = 'test@GMAIL.COM'
        user = get_user_model().objects.create_user(
            email, 'test@8848'
        )
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """ Test creating  users with no email raises error """
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test@8848')

    def test_create_new_superuser(self):
        """ Test Creating new superuser """
        user = get_user_model().objects.create_superuser(
            'test@gmail.com',
            'test@8848'
        )

        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)

    def test_tag_str(self):
        """ Test tag string representations """
        tag = models.Tag.objects.create(
            user=sample_user(),
            name='daju'
        )
        self.assertEqual(str(tag), tag.name)

    def test_ingredients_str(self):
        """ Test the ingredients string representations """
        ingredients = models.Ingredient.objects.create(
            user=sample_user(),
            name='Cucumber'
        )

        self.assertEqual(str(ingredients), ingredients.name)
