from django.urls import path

from .views import CreateUserView, CreateTokenView, ManageUserView

app_name = 'users'

urlpatterns = [
    # api/user/create(endpoint)
    path('create/', CreateUserView.as_view(), name='create'),
    # api/user/token(endpoint)
    path('token/', CreateTokenView.as_view(), name='token'),
    # api/user/me(endpoint)
    path('me/', ManageUserView.as_view(), name='me'),
]
